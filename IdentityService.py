# -*- coding: utf-8 -*-
"""
Created on Mon Jan 13 10:04:11 2020

@author: Vojtech Bednar 
"""


from typing import Dict, Any
import os
import json
import sys
import getpass

#-------------------------------------------------------------------------------    
#                             class IdentityService -->
#-------------------------------------------------------------------------------    
class IdentityService:        
    #--------------------------------------------------------------------------
    #                                     ctor
    #---------------------------------------------------------------------------
    def __init__(self):
        self.dataList = list()

    #--------------------------------------------------------------------------
    #                                   register()
    #--------------------------------------------------------------------------
    # Raises exception if user already exists
    #--------------------------------------------------------------------------                
    def register(self, username: str, password: str, properties: Dict[str, Any]):        
        userRecord = self._find_user_record(username)
        if not userRecord is None:
            raise UserExistsException("User {} (up to casing) already exists".format(username))           
        encPassword = self._encrypt(password)
        userRecord = [username, encPassword, properties]
        self.dataList.append(userRecord)
        
    #--------------------------------------------------------------------------
    #                                  authenticate()
    #---------------------------------------------------------------------------    
    def authenticate(self, username: str, password: str) -> bool:
        userRecord = self._find_user_record(username)
        if userRecord is None:
            return False        
        storedPassword = self._decrypt(userRecord[1])
        return password == storedPassword
        
    #--------------------------------------------------------------------------
    #                                 save_to_json()
    #---------------------------------------------------------------------------
    def save_to_json(self, path: str, overwrite: bool = False):
        if overwrite == False and os.path.exists(path):
            raise Exception("Path (file) {} already exists".format(path))                
        data = {}
        data['users'] = self.dataList      
        #print(data)
        with open(path, 'w', encoding='utf-8') as outfile:
            json.dump(data, outfile, ensure_ascii=False) 
    
    #--------------------------------------------------------------------------
    #                                 load_from_json()
    #---------------------------------------------------------------------------      
    def load_from_json(self, path:str):
        with open(path, 'r', encoding='utf-8') as infile:
            data = json.load(infile)
        self.dataList = data['users']
            
    #--------------------------------------------------------------------------
    #                             _find_user_record()
    #--------------------------------------------------------------------------
    # Find user record, return None if not found             
    # Comparison in search is case insensitive        
    #--------------------------------------------------------------------------
    def _find_user_record(self, username: str) -> []: #Username first in []
        usernameLo = username.lower()
        for userRecord in self.dataList:            
            if userRecord[0].lower() == usernameLo:
                return userRecord
        return None    
    
    #--------------------------------------------------------------------------
    #                             _encrypt()
    #--------------------------------------------------------------------------
    #  (password) encryption, TBD
    #--------------------------------------------------------------------------
    def _encrypt(self, text: str) -> str:
        return text
    
    #--------------------------------------------------------------------------
    #                             _decrypt()
    #--------------------------------------------------------------------------
    #  (password) decryption, TBD
    #--------------------------------------------------------------------------
    def _decrypt(self, text: str) -> str:
        return text

#-------------------------------------------------------------------------------    
#                        <--   class IdentityService
#-------------------------------------------------------------------------------        

class UserExistsException(Exception):
  pass    

#-------------------------------------------------------------------------------    
#                             class Cli4IdentityService -->
#-------------------------------------------------------------------------------     
# Cli for identity service. See method print_help() for function description.
# After construction, method perform() is an entry point
#-------------------------------------------------------------------------------      
class Cli4IdentityService:
    #--------------------------------------------------------------------------
    #                                     ctor
    #---------------------------------------------------------------------------
    def __init__(self):
        self.defaultPath = "DefaultUsers.json"          
        #Default path for json file with data        

    #--------------------------------------------------------------------------
    #                                   perform()
    #--------------------------------------------------------------------------
    #  Parse command line arguments and perform registration or authentication 
    #  based on them. 
    #--------------------------------------------------------------------------    
        
    def perform(self):
        length = len(sys.argv)
        if length == 1:
            self.print_help("")
            return
        if length > 3:
            self.print_help("At most two positional arguments expected.")
            return
        if length == 3:
            path = sys.argv[2]       
        else: path =  self.defaultPath
    
        if sys.argv[1] == "--register" or sys.argv[1] == "-r":
            self.register(path)
        elif sys.argv[1] == "--authenticate" or sys.argv[1] == "-a":
            self.authenticate(path)
        else:
            self.print_help("Unknown argument: {}".format(sys.argv[1]))
            
    #--------------------------------------------------------------------------
    #                             register()
    #--------------------------------------------------------------------------    
    def register(self, path: str):            
        print("New user registration")
        name = self.input_name("Name:")
        
        while True:
            P1 = self.input_password("Password:")
            P2 = self.input_password("Retype password:")
            if P1 == P2:
                password = P1
                break
            else:
                print("\nPassword does not match. Please enter password again.")
        try:
            self.register_to_is(name, password, path)
        except UserExistsException:
           print("Error: {}".format(sys.exc_info()[1]))     
           return
       
        print("User successfully registered.")
            
    #--------------------------------------------------------------------------
    #                             input_name()
    #--------------------------------------------------------------------------
    def input_name(self, Message: str) -> str:
        print("{} ".format(Message), end="")
        name = input()
        return name
    
    #--------------------------------------------------------------------------
    #                             input_password()
    #--------------------------------------------------------------------------
    def input_password(self, Message: str) -> str:        
        pwd = getpass.getpass(Message)
        return pwd
    
    #--------------------------------------------------------------------------
    #                             register_to_is()
    #--------------------------------------------------------------------------
    def register_to_is(self, name: str, password: str, path):
        Is = IdentityService()
        if os.path.exists(path):
            Is.load_from_json(path)
        Is.register(name, password, None)
        Is.save_to_json(path, overwrite = True)
            
    #--------------------------------------------------------------------------
    #                             authenticate()
    #--------------------------------------------------------------------------   
    def authenticate(self, path: str):
        print("Authentication")
        name = self.input_name("Name:")
        pwd = self.input_password("Password:")
        Is = IdentityService()
        Is.load_from_json(path)        
        if Is.authenticate(name, pwd):
            print("Authentication OK.")
        else:
            print("Authentication failed")
        
    #--------------------------------------------------------------------------
    #                             print_help()
    #--------------------------------------------------------------------------
    def print_help(self, errMsg: str):                
        Text = "Identity service\n"
        if errMsg.strip() != "":
            Text += errMsg + "\n"
        Text += "Usage: \n"
        Text += "IdentityService <command> <path>\ncommand:\n"
        Text += " --register or -r: Register a new user. (Prompt for entering data will appear.)\n" 
        Text += " --authenticate or -a: Authenticate user. (Prompt will appear.)\n" 
        Text += "path:"
        Text += " Path to data file, not mandatory. (If not entered, default file will be used.)"
        
        print(Text)    

#-------------------------------------------------------------------------------    
#                    <--     class Cli4IdentityService 
#-------------------------------------------------------------------------------     

#-------------------------------------------------------------------------------    
#                                   Main()
#-------------------------------------------------------------------------------    
def Main():
    Cli = Cli4IdentityService()
    Cli.perform()    

#-------------------------------------------------------------------------------    
#-------------------------------------------------------------------------------        
if __name__ == "__main__":
    Main()