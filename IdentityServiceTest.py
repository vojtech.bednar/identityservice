# -*- coding: utf-8 -*-
"""
Created on Mon Jan 13 17:12:49 2020

@author: Vojta
"""


import IdentityService
import unittest


class IsTestCase(unittest.TestCase):
    
    def test_register_authenticate_positive1(self):
        Is = IdentityService.IdentityService()
        Is.register("Mirek", "Meslo", None) 
        Is.register("Pepa", "Peslo", None) 
        self.assertTrue(Is.authenticate("Mirek", "Meslo"))
    
    def test_register_authenticate_positive2(self):
        Is = IdentityService.IdentityService()
        Is.register("Mirek", "Meslo", None) 
        Is.register("Pepa", "Peslo", None) 
        self.assertTrue(Is.authenticate("Pepa", "Peslo"))
    
    
    def test_register_authenticate_negative_l(self):
        Is = IdentityService.IdentityService()
        Is.register("Mirek", "Meslo", None) 
        Is.register("Pepa", "Peslo", None) 
        self.assertFalse(Is.authenticate("Janek", "Heslo"))
        
        
    def test_register_authenticate_negative_p1(self):
        Is = IdentityService.IdentityService()
        Is.register("Mirek", "Meslo", None) 
        Is.register("Pepa", "Peslo", None) 
        self.assertFalse(Is.authenticate("Pepa", "Meslo"))
        
    def test_register_authenticate_negative_p2(self):
        Is = IdentityService.IdentityService()
        Is.register("Mirek", "Meslo", None) 
        Is.register("Pepa", "Peslo", None) 
        self.assertFalse(Is.authenticate("Pepa", "ABC"))        
        
    def test_register_authenticate_negative_pempty(self):
        Is = IdentityService.IdentityService()
        Is.register("Mirek", "Meslo", None) 
        Is.register("Pepa", "Peslo", None) 
        self.assertFalse(Is.authenticate("Pepa", ""))            
    
    def test_register_twice(self):
        Is = IdentityService.IdentityService()
        with self.assertRaises(IdentityService.UserExistsException):
            Is.register("Mirek", "Meslo", None) 
            Is.register("Mirek", "Meslo", None) 
    
    def test_save_load(self):
        Is1 = IdentityService.IdentityService()
        Is1.register("Mirek", "Meslo", None) 
        Is1.register("Pepa", "Peslo", None) 
        Is1.save_to_json("Test.json", overwrite = True)
        
        Is2 = IdentityService.IdentityService()
        Is2.load_from_json("Test.json")
        self.assertTrue(Is2.authenticate("Pepa", "Peslo"))
        

def suite():
    suite = unittest.TestSuite()
    suite.addTest(IsTestCase('test_register_authenticate_positive1'))
    suite.addTest(IsTestCase('test_register_authenticate_positive2'))
    suite.addTest(IsTestCase('test_register_authenticate_negative_l'))
    suite.addTest(IsTestCase('test_register_authenticate_negative_p1'))
    suite.addTest(IsTestCase('test_register_authenticate_negative_p2'))
    suite.addTest(IsTestCase('test_register_authenticate_negative_pempty'))
    suite.addTest(IsTestCase('test_register_twice'))
    suite.addTest(IsTestCase('test_save_load'))
    
    return suite

if __name__ == '__main__':
    runner = unittest.TextTestRunner()
    runner.run(suite())
